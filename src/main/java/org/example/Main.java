package org.example;

public class Main {
    private Main() {
        // Приватний конструктор для утилітного класу
    }
    /**
     * Головний метод програми.
     *
     * @param args аргументи командного рядка
     */
    public static void main(final String[] args) {

        System.out.println("Hello world!");

    }
}